﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ValueUpdater : MonoBehaviour
{
    private Action<float> updateFunction; 
    private float t=0;
    private float duration;
    public static void Run(GameObject host, float _duration, Action<float> _updateFunction)
    {
        var valueUpdater = host.AddComponent<ValueUpdater>() as ValueUpdater;
        valueUpdater.duration = _duration;
        valueUpdater.updateFunction= _updateFunction;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        updateFunction(t/duration);
        t += Time.deltaTime;
        if (t>duration)
            Destroy(this);
    }
}
