﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ProgressBarController : MonoBehaviour, IProgressBarEventManager
{
    public string progressBarName="unknown";
    public float mouseDownPos=0.3f;
    public float mouseUpPos=0.7f;
    private RectTransform[] bars;
    private UnityEngine.UI.Image image;
    private float timeToEnd = 0;
    private float wholeTime = 0;
    private bool go = false;
    private float rotationAngle = 0;
    private GameObject responseTo;
    private float mouseDown = 0;
    private float mouseUp = 1;
    private void MouseDown()
    {

        if (rotationAngle > 1 && Input.GetMouseButtonDown(0))
        {
            mouseDown = timeToEnd / wholeTime;
            bars[5].anchoredPosition = new Vector2( mouseDown * 500, -18);
            bars[5].gameObject.SetActive(true);
        }

        if (rotationAngle <= 0 && Input.GetMouseButtonDown(1))
        {
            mouseDown = timeToEnd / wholeTime;
            bars[5].anchoredPosition = new Vector2((1-mouseDown) * 500+500, -18);
            bars[5].gameObject.SetActive(true);
        }
            
    }

    private void MouseUp()
    {
        if (rotationAngle > 1 && Input.GetMouseButtonUp(0))
        {
            mouseUp = timeToEnd / wholeTime;
            bars[6].anchoredPosition = new Vector2(mouseUp * 500, -18);
            bars[6].gameObject.SetActive(true);
        }
            

        if (rotationAngle <= 0 && Input.GetMouseButtonUp(1))
        {
            mouseUp = timeToEnd / wholeTime;
            bars[6].anchoredPosition = new Vector2((1-mouseUp) * 500+500, -18);
            bars[6].gameObject.SetActive(true);
        }
           
    }

    

    void Start()
    {
        
        bars = this.gameObject.GetComponentsInChildren<RectTransform>() as RectTransform[];
        image= (this.gameObject.GetComponentsInChildren<UnityEngine.UI.Image>() as UnityEngine.UI.Image[])[1];
        bars[1].gameObject.SetActive(false);
        float width = (mouseUpPos - mouseDownPos) * 500;

        bars[2].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        bars[2].anchoredPosition = new Vector2((1 - mouseUpPos) * 500, 0);
        
        bars[3].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        bars[3].anchoredPosition = new Vector2(mouseDownPos * 500+500, 0);

        bars[5].gameObject.SetActive(false);
        bars[6].gameObject.SetActive(false);
    }

    public void RocketRotationWarning(float time, float angle, GameObject response)
    {
        timeToEnd = time;
        wholeTime = time;
        go = true;
        bars[1].gameObject.SetActive(true);
        rotationAngle = angle;
        responseTo = response;
    }
    void Update()
    {
        if (go)
        {
            UpdateBar();
            timeToEnd -= Time.deltaTime;
            StopBarUpdateIfEndAppears();
            MouseDown();
            MouseUp();
        }
    }

    private void StopBarUpdateIfEndAppears()
    {
        if (timeToEnd < 0)
        {
            go = false;
            timeToEnd = 0;

            bars[1].gameObject.SetActive(false);
            bars[0].SetPositionAndRotation(bars[0].position, Quaternion.identity);
            ExecuteEvents.Execute<IRocketEventManager>(responseTo, null,
                (x, y) =>
                {
                    float multiplier = Mathf.Abs(1 - mouseUp - mouseUpPos) * 0.5f + Mathf.Abs(1 - mouseDown - mouseDownPos) * 0.5f;
                    x.MakeRotation(multiplier);
                }
            );
        }
    }

    private void UpdateBar()
    {
        float barPos = (1 - timeToEnd / wholeTime);
        bars[1].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 1000 * barPos);
        bars[0].SetPositionAndRotation(bars[0].position, Quaternion.Euler(0, 0, rotationAngle * barPos));
        image.color = new Color(barPos, 1 - barPos, 0);
    }
}
