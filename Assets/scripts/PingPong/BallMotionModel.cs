﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model
{
    [Serializable]
    public class BallMotionModel
    {
        [SerializeField]
        private float mass = 0.5f;
        [SerializeField]
        private Vector3 velocity;
        [SerializeField]
        private Vector3 position;
        [SerializeField]
        private Vector3 gravity = new Vector3(0, -9.87f, 0);
        [SerializeField]
        private float ground = 0;
        [SerializeField]
        private float minVelocity = 0.05f;
        public BallMotionModel()
        {
            velocity = new Vector3(0, 0, 0);
            position = new Vector3(0, 3, 0);
        }

        public BallMotionModel(Vector3 startVelocity, Vector3 startPosition, float _ground)
        {
            velocity = startVelocity;
            position = startPosition;
            ground = _ground;
        }

        public Vector3 Position { get => position; set => position = value; }
        public float MinVelocity { get => minVelocity; set => minVelocity = value; }

        public void Update(float time)
        {
            velocity += gravity * mass * (position.y - ground) * time;
            position += velocity * time;
        }

        public void Hit(float absorptionFactor, Vector3 reflectionNormal)
        {
            if (absorptionFactor < 0 || absorptionFactor > 1)
                throw new ArgumentOutOfRangeException("[Error 0001]: Absorption factor for ball motion model is out of range. Is: " + absorptionFactor.ToString() + " should be in range [0,1]");
            if (velocity.magnitude < minVelocity)
                velocity = Vector3.zero;
            else
            {
                velocity = Vector3.Reflect(velocity, reflectionNormal) * (1 - absorptionFactor);
            }
        }
    }
}
    
