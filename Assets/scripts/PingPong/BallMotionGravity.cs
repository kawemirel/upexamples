﻿using Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BallMotionGravity : MonoBehaviour, IRocketEventManager
{
    #region parameters
    private BallMotionModel motionModel;
    public Vector3 StartPosition = new Vector3(0, 3, 0);
    public Vector3 StartVelocity = new Vector3(0, 0, 0);
    public float absorptionFactor=0;
    public float minWaitTime = 0.5f;
    public float maxWaitTime= 3.0f;
    public float rotationChance=0.2f;
    public float maxRotationSpeed;
    public float minRotationSpeed;
    public float deletePosition=0;
    public float maxWarningTime;
    public float minWarningTime;
    public bool ballWithRigidBody = false;
    #endregion
    #region private fields
    private float rotationSpeed;
    private GameObject ball;
    private float waitTime = 0;
    private bool canRotate = false;
    private bool wait = true;
    private ProgressBarController progressBar;
    private float angle;
    #endregion
    #region overrided methods: Start, (Fixed)Update, collision service...
    void Start()
    {
        CreateBall();
        FindRotationWarningProgressBar();
    }
    void Update()
    {
        motionModel.Update(Time.deltaTime);
        ball.transform.position = motionModel.Position;
        if (ball.transform.position.y < deletePosition)
            RespawnBall();
        if (!canRotate)
            if (waitTime <= 0)
                StartRotationRandomizing();
        if (wait)
            waitTime -= Time.deltaTime;
    }
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("OnTrigger");
        if (other.gameObject == ball)
            motionModel.Hit(absorptionFactor, this.transform.up);
    }
    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("OnCollision");
        if (collision.gameObject == ball)
            motionModel.Hit(absorptionFactor, this.transform.up);
    }
    #endregion
    private void FixedUpdate()
    {
        if (canRotate)
        {
            Quaternion tr = Quaternion.Euler(angle, 0, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, tr, Time.fixedDeltaTime * rotationSpeed);
            
            if (transform.rotation == tr)
            {
                canRotate = false;
                wait = true;
            }
        }
    }
    
    #region create, respawn, find rotation...
    private void FindRotationWarningProgressBar()
    {
        ProgressBarController[] progressBars = FindObjectsOfType<ProgressBarController>() as ProgressBarController[];
        foreach (var pBar in progressBars)
        {
            if (pBar.progressBarName == "RocketRotationWarning")
            {
                progressBar = pBar;
                break;
            }
        }
    }
    private void CreateBall()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        ball = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        ball.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        ball.transform.position = this.transform.position + new Vector3(0, 0.15f, 0) + StartPosition;
        Renderer ballRenderer = ball.GetComponent<Renderer>() as Renderer;
        if (!ballRenderer)
            ballRenderer = ball.AddComponent<Renderer>() as Renderer;

        ballRenderer.material = Resources.Load<Material>("materials/PingPongMaterial");
        if (ballWithRigidBody)
            ball.AddComponent<Rigidbody>();

        motionModel = new BallMotionModel(StartVelocity, ball.transform.position, 0.15f);
    }
    private void RespawnBall()
    {
        canRotate = false;
        this.transform.rotation = Quaternion.identity;
        GameObject.Destroy(ball);
        CreateBall();
        wait = true;
        waitTime = maxWaitTime;
    }
    #endregion
    private void StartRotationRandomizing()
    {
        var r = Random.Range(0.0f, 1.0f);
        if (r > 1 - rotationChance)
        {
            wait = false;
            angle = Random.Range(-45.0f, 45.0f);
            rotationSpeed = Random.Range(minRotationSpeed, maxRotationSpeed);
            waitTime = Random.Range(minWaitTime, maxWaitTime);
            ExecuteEvents.Execute<IProgressBarEventManager>(progressBar.gameObject, null,
                (x, y) =>
                {
                    x.RocketRotationWarning(Random.Range(minWarningTime, maxWarningTime), -angle, gameObject);
                }
            );
        }
    }
    public void MakeRotation(float multiplier)
    {
        angle = angle * multiplier;
        canRotate = true;
    }
}
