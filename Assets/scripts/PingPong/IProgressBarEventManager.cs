﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IProgressBarEventManager : IEventSystemHandler
{
    void RocketRotationWarning(float time, float angle, GameObject response);
}

public interface IRocketEventManager :IEventSystemHandler
{
    void MakeRotation(float multiplier);
}
