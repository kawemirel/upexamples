﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMotion : MonoBehaviour
{
    public bool jestFajny;
    public float height = 2.0f;
    [SerializeField]
    private float speed = 1.0f;
    private float direction = 1;
    private GameObject ball;
    // Start is called before the first frame update
    void Start()
    {
        
        ball = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        
        ball.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        ball.transform.position = this.transform.position + new Vector3(0, 0.15f, 0);
        //Adding the material from asset (safe metod)
        Renderer ballRenderer = ball.GetComponent<Renderer>() as Renderer;
        
        if (!ballRenderer)
            ballRenderer = ball.AddComponent<Renderer>() as Renderer;

        ballRenderer.material = Resources.Load<Material>("materials/PingPongMaterial");
    }

    // Update is called once per frame
    void Update()
    {
        ball.transform.position += new Vector3(0, Time.deltaTime * speed, 0)*direction;
        if (ball.transform.position.y >= height)
            direction = -1;
        if (ball.transform.position.y <= (this.transform.position + new Vector3(0, 0.15f, 0)).y)
            direction = 1;
    }
}
