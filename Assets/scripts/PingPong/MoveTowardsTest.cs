﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsTest : MonoBehaviour
{
    GameObject ball;
    public Vector3 StartPosition;
    public Vector3 EndPosition;
    public float distance;
    public Vector3 axis;

    // Start is called before the first frame update
    void Start()
    {
        CreateBall();
        axis = ball.transform.forward;
    }
    private void CreateBall()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        ball = GameObject.CreatePrimitive(PrimitiveType.Cube);
        ball.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        ball.transform.position = this.transform.position + StartPosition;
        Renderer ballRenderer = ball.GetComponent<Renderer>() as Renderer;
        if (!ballRenderer)
            ballRenderer = ball.AddComponent<Renderer>() as Renderer;

        ballRenderer.material = Resources.Load<Material>("materials/PingPongMaterial");
        
        var rb=ball.AddComponent<Rigidbody>();
        rb.useGravity = false;
        rb.isKinematic = true;
        
        var go= GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        go.transform.localScale = new Vector3(0.2f, 1f, 0.2f);
        go.transform.rotation = Quaternion.Euler(90, 45, 0);
        //go.transform.parent = ball.transform;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            /*Vector3 pos = ball.transform.position;
            ball.transform.position = Vector3.MoveTowards(ball.transform.position, EndPosition, 0.05f );
            distance = (pos - ball.transform.position).magnitude;*/

            Vector3 newDirection = Vector3.RotateTowards(ball.transform.forward, EndPosition, Mathf.PI/50, 0.0f);
            ball.transform.rotation = Quaternion.LookRotation(newDirection);
            axis = ball.transform.forward;
            
        }
    }
}
