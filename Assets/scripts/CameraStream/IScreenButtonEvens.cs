﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IScreenButtonEvents : IEventSystemHandler
{
    void OnMouseClick();
    void OnTurnOn();
    void OnTurnOff();
}
