﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseService : MonoBehaviour
{
    public enum TurnOffButtonStatus
    {
        IS_ON,
        IS_OFF
    }
    public Color MouseOverColor;
    public Color TurnOnColor;
    public Color TurnOffColor;
    public Vector3 TurnOnChange;
    private Renderer renderer;
    private Color origColor;
    private Vector3 origPosition;
    private Color currentColor;
    private DisplayManager dManager;
    
    [SerializeField]
    private TurnOffButtonStatus status = TurnOffButtonStatus.IS_OFF;
    public TurnOffButtonStatus Status { get => status; set => status = value; }
    public DisplayManager DManager {set => dManager = value; }

    private void Start()
    {
        renderer = GetComponent<Renderer>() as Renderer;
        if (!renderer)
            Debug.Log("Renderer is not present in turn-on Display's button");
    }
    private void OnMouseEnter()
    {
        ValueUpdater.Run(this.gameObject, 0.25f,
            t => { renderer.material.color=Color.Lerp(renderer.material.color, (MouseOverColor+origColor)/2, t); });
    }

    private void OnMouseExit()
    {
        //renderer.material.color = origColorMouseOver;
        if (status==TurnOffButtonStatus.IS_OFF)
            ValueUpdater.Run(this.gameObject, 0.25f,
                t => { renderer.material.color = Color.Lerp((MouseOverColor+ TurnOffColor)/2, TurnOffColor, t); });
        else
            ValueUpdater.Run(this.gameObject, 0.25f,
                t => { renderer.material.color = Color.Lerp((TurnOnColor + MouseOverColor) / 2, TurnOnColor, t); });
    }

    private void OnMouseUpAsButton()
    {
        if (status == TurnOffButtonStatus.IS_OFF)
        {
            origPosition = this.gameObject.transform.position;
            currentColor = renderer.material.color;
            ValueUpdater.Run(this.gameObject, 0.25f,
                t => 
                {
                    this.gameObject.transform.position = Vector3.Lerp(origPosition, origPosition+TurnOnChange, t);
                    renderer.material.color = Color.Lerp(currentColor, (TurnOnColor+ MouseOverColor) / 2, t);
                });
            dManager.StartStreaming();
            status = TurnOffButtonStatus.IS_ON;
            
        }
        else
        {
            dManager.Pause();
            currentColor = renderer.material.color;
            ValueUpdater.Run(this.gameObject, 0.25f,
                t => 
                {
                    this.gameObject.transform.position = Vector3.Lerp(TurnOnChange+ origPosition, origPosition, t);
                    renderer.material.color = Color.Lerp(currentColor, (TurnOffColor + MouseOverColor) / 2, t);
                });
            
            status = TurnOffButtonStatus.IS_OFF;
            
        }
    }
}
