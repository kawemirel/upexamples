﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class DrawFigure : MonoBehaviour
{
    [DllImport("OCVWrapper", EntryPoint = "detectFace", CallingConvention = CallingConvention.StdCall)]
    public static extern void detectFace(long length, int width, int height, byte[] data, out float tlx, out float tly, out float brx, out float bry);
    [DllImport("OCVWrapper", EntryPoint = "loadClassifier", CallingConvention = CallingConvention.StdCall)]
    public static extern void loadClassifier();
    public enum DrawFigureType
    {
        Circle,
        Lens,
        Classifier
    }
    public DrawFigureType drawType=DrawFigureType.Lens;

    public float rollSpeed = 1;
    public float magnifySpeed=0.2f;
    public float toleranceChangeSpeed = 0.01f;
    private Renderer renderer;
    private float aspect;
    private bool canChange = false;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>() as Renderer;
        if (!renderer)
            Debug.Log("There is no renderer on the screen!");
        string shaderName = "LensShader";
        if (drawType == DrawFigureType.Circle || drawType == DrawFigureType.Classifier)
            shaderName = "DrawCircleShader";
        renderer.material.shader = Shader.Find(shaderName);
        renderer.sharedMaterial.SetFloat("CX", 0.5f);
        renderer.sharedMaterial.SetFloat("CY", 0.5f);
        renderer.sharedMaterial.SetFloat("R", 0.2f);
        if (drawType == DrawFigureType.Circle || drawType == DrawFigureType.Classifier)
            renderer.sharedMaterial.SetFloat("W", 0.03f);
        if (drawType == DrawFigureType.Lens)
            renderer.sharedMaterial.SetFloat("M", 2.0f);
        aspect = transform.localScale.x / transform.localScale.y;
        renderer.sharedMaterial.SetFloat("A", aspect);
        if (drawType == DrawFigureType.Classifier)
            loadClassifier();
        renderer.sharedMaterial.SetInt("IsOn", 0);
    }
    private void OnMouseDown()
    {
        canChange = true;
        renderer.sharedMaterial.SetInt("IsOn", 1);
    }

    private void OnMouseUp()
    {
        canChange = false;
        renderer.sharedMaterial.SetInt("IsOn", 0);
    }

    private void OnMouseOver()
    {
        if (canChange)
        {
            MousePositionOnScreen();
            MouseScrollService();
            MagnToleranceChange();
            Classify();
        }
    }

    private void Classify()
    {
        if (drawType == DrawFigureType.Classifier)
        {

            Texture2D t = new Texture2D(renderer.material.mainTexture.width, renderer.material.mainTexture.height, TextureFormat.RGB24, false);
            t.SetPixels((renderer.material.mainTexture as WebCamTexture).GetPixels());
            t.Apply();
            byte[] bytes = t.GetRawTextureData();
            float tlx = 0, tly = 0, brx = 0, bry = 0;
            detectFace(bytes.Length, t.width, t.height, bytes, out tlx, out tly, out brx, out bry);
            Debug.Log("FaceCoords:(" + tlx + ", " + tly + ", " + brx + ", " + bry + ",) ");
            renderer.sharedMaterial.SetFloat("CX", 1-(tlx + brx / 2) / t.width);
            renderer.sharedMaterial.SetFloat("CY", (tly + bry / 2) / t.height);
            renderer.sharedMaterial.SetFloat("R", bry/(2*t.height));
        }
    }

    private void MousePositionOnScreen()
    {
        if (drawType != DrawFigureType.Classifier)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    renderer.sharedMaterial.SetFloat("CX", hit.textureCoord.x);
                    renderer.sharedMaterial.SetFloat("CY", hit.textureCoord.y);
                }
            }
        }
    }

    private void MagnToleranceChange()
    {
        if (drawType != DrawFigureType.Classifier)
        {
            if (Input.GetKeyUp(KeyCode.Z))
            {
                string name = "M";
                if (drawType == DrawFigureType.Circle)
                    name = "W";
                float M = renderer.sharedMaterial.GetFloat(name);
                M += rollSpeed;
                renderer.sharedMaterial.SetFloat(name, M);
            }

            if (Input.GetKeyUp(KeyCode.X))
            {
                string name = "M";
                if (drawType == DrawFigureType.Circle)
                    name = "W";
                float M = renderer.sharedMaterial.GetFloat(name);
                M -= rollSpeed;
                renderer.sharedMaterial.SetFloat(name, M);
            }
        }
    }

    private void MouseScrollService()
    {
        if (drawType != DrawFigureType.Classifier)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
            {
                float R = renderer.sharedMaterial.GetFloat("R");
                R += rollSpeed * Time.deltaTime;
                renderer.sharedMaterial.SetFloat("R", R);
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
            {
                float R = renderer.sharedMaterial.GetFloat("R");
                R -= rollSpeed * Time.deltaTime;
                renderer.sharedMaterial.SetFloat("R", R);
            }
        }
    }
}
