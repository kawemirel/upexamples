﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayManager : MonoBehaviour
{
    private PhysicalCameraStreamer camStreamer;
    private MouseService[] buttons;
    // Start is called before the first frame update
    void Start()
    {
        //Setting up the cam streamer. We assume, that it already is a component of buttons, 
        //that are children of Game Object having DisplayManager, and there is only one such an object. 
        camStreamer = GetComponentInChildren<PhysicalCameraStreamer>() as PhysicalCameraStreamer;
        if (!camStreamer)
            Debug.Log("PhysicalCameraStreamer component not found in children of Display!");

        //Setting up the buttons:
        buttons=GetComponentsInChildren<MouseService>() as MouseService[];
        DisplayManager me= GetComponent<DisplayManager>();
        foreach (var btn in buttons)
            btn.DManager = me;
    }

    public void StartStreaming() => camStreamer.Play();
    public void Pause() => camStreamer.Pause();
    public void Stop() => camStreamer.Stop();
}
