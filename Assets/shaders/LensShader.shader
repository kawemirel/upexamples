﻿Shader "LensShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		CX("Center-X", Float) = 30
		CY("Center-Y", Float) = 130
		R("Radius", Float) = 20
		A("AspectRatio", Float) = 1
		M("Magification", Float) = 2.0
		IsOn("IsOn", Int) = 0
	}
	SubShader
	{
		Pass{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		v2f vert(appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = v.uv;
			return o;
		};

		sampler2D _MainTex;
		float CX;
		float CY;
		float R;
		float A;
		float M;
		float IsOn;
		fixed4 frag(v2f i) : SV_Target
		{
			fixed4 col = tex2D(_MainTex, float2(i.uv.x, 1-i.uv.y));
			if (IsOn == 1) {
				float X = CX * A;
				float x = i.uv.x*A;
				float y = i.uv.y;
				float Left = (x - X)*(x - X) + (y - CY)*(y - CY);
				float Right = R * R;
				if (Left >= (1 - 0.1f)*Right && Left <= (1 + 0.1f)*Right)
				{
					col = fixed4(1, 0, 0, 1);
				}
				if (Left <= Right)
				{
					float X = (i.uv.x - CX) / M;
					float Y = (i.uv.y - CY) / M;
					col = tex2D(_MainTex, float2(CX+X   , 1-CY- Y));
				}
			}
			return col;
		}
		ENDCG
		}
	}
}
